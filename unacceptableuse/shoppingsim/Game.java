package unacceptableuse.shoppingsim;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import unacceptableuse.shoppingsim.screen.Screen;
import unacceptableuse.shoppingsim.screen.ScreenStart;

public class Game extends Canvas {

	private Graphics g;
	private boolean running = true;
	private BufferStrategy bs;
	private Screen currentScreen;
	private Color background = new Color(105, 105, 255);
	public SpriteSheet sheetUI, sheetItems;
	public Font font;
	
	public ItemRegistry iRegistry;
	


	public Game() {
		System.out.println("Starting...");
		JFrame frame = new JFrame();
		frame.setTitle("Test");
		frame.add(this);
		frame.setSize(800, 1000);
		this.setIgnoreRepaint(true);
		frame.setResizable(false);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.createBufferStrategy(2);
		bs = this.getBufferStrategy();
		g = bs.getDrawGraphics();

		init();

	}

	private void init() {
		System.out.println("Loading font...");
		
		System.out.println("Loading images...");
		try {
			font = new Font(ImageIO.read(new File("res/font.png")),g, this);
			sheetUI = new SpriteSheet(ImageIO.read(new File("res/ui.png")), 16);
			System.out.println(sheetUI);
		} catch (IOException e) {
			throw new RuntimeException("One or more spritesheets could not be loaded!");
		}
		initItems();
		currentScreen = new ScreenStart();
		gameLoop();
	}

	private void initItems() {
		System.out.println("Registering Items");
		iRegistry = new ItemRegistry();

		iRegistry.registerItem(new Item().setWeight(600).setName("Item #1")
				.setFragility(10).setTexpos(0));
		iRegistry.registerItem(new Item().setWeight(200).setName("Item #2")
				.setFragility(6).setTexpos(1));
		iRegistry.registerItem(new Item().setWeight(700).setName("Item #3")
				.setFragility(8).setTexpos(2));


	}

	private void gameLoop() {

		long lastTime = System.nanoTime();
		double unprocessed = 0;
		double nsPerTick = 1000000000.0 / 60.0;
		int frames = 0;
		int ticks = 0;
		long lastTimer1 = System.currentTimeMillis();

		while (running) {
			long now = System.nanoTime();
			unprocessed += (now - lastTime) / nsPerTick;
			lastTime = now;
			while (unprocessed >= 1) {
				ticks++;
				tick();
				unprocessed -= 1;

			}
			{
				frames++;
				render();
			}

			try {
				Thread.sleep((int) ((1 - unprocessed) * 1000 / 60));
			} catch (InterruptedException e) {

				e.printStackTrace();
			}

			if (System.currentTimeMillis() - lastTimer1 > 1000) {
				lastTimer1 += 1000;
			//	System.out.println(ticks + " ticks " + frames + " fps");
				ticks = 0;
				frames = 0;

			}

		}
	}

	private void tick() {
		
		currentScreen.tick(this);

	}

	private void render() {

		g.setColor(background);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());

		currentScreen.render(g);

		bs.show();
		//g.dispose();
	}

	public static void main(String[] args) {
		new Game();
	}
}
