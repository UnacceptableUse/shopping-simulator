package unacceptableuse.shoppingsim.screen;

import java.awt.Graphics;
import java.util.Random;

import unacceptableuse.shoppingsim.Game;
import unacceptableuse.shoppingsim.Item;

public class ScreenGame extends Screen{
	
	
	
	
	Random rand = new Random();
	
	Item[] que = new Item[10];
	
	public void ScreenGame()
	{
		
	}
	
	
	private Item randomItem(Game game)
	{
		int randItem = rand.nextInt(game.iRegistry.getRegistrySize());
		return game.iRegistry.getItem(randItem);
	}
	
	@Override
	public void render(Graphics g) {
		
		
	}

	@Override
	public void tick(Game game) {
	
		for(int i = 0; i < que.length; i++)
		{
			if(que[i] == null)
			{
				que[i] = randomItem(game);
				System.out.println("Item position: "+i+". Item name: "+que[i].getName());
			}
		}
	}

}
