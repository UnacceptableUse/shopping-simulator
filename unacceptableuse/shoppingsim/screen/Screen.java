package unacceptableuse.shoppingsim.screen;

import java.awt.Graphics;

import unacceptableuse.shoppingsim.Game;

public abstract class Screen {

	
	
	
	
	public abstract void render(Graphics g);
	
	public abstract void tick(Game game);
}
