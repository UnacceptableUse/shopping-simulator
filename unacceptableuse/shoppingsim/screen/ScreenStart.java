package unacceptableuse.shoppingsim.screen;

import java.awt.Color;
import java.awt.Graphics;

import unacceptableuse.shoppingsim.Game;

public class ScreenStart extends Screen {

	private Game game;

	private int coinTime = 0, sequenceTime = 0;

	public void render(Graphics g) {

		drawLogoSeqence(g, 10, 10);
		drawInsertCoin(g, 350, 640, 32);
	}

	public void tick(Game game) {
		this.game = game;
	}

	private void drawInsertCoin(Graphics g, int x, int y, int size) {
		if (game != null) {
			coinTime++;
			if (coinTime >= 60) {
				if (coinTime == 120)
					coinTime = 0;
				game.font.drawString("Insert Coin!", x, y, size, new Color(255, 55, 55));
			}
		}
	}

	private void drawLogoSeqence(Graphics g, int x, int y) {
		int size = 64;
		// sequenceTime++;

		if (game != null) {
			drawShopping(g, x, y, size);
			drawSimulator(g, x + size, y + size, size);
			
		}
		

	}

	private void drawShopping(Graphics g, int x, int y, int size) {
		g.drawImage(game.sheetUI.getImage(3), x, y, size, size, game);
		g.drawImage(game.sheetUI.getImage(4), x + (size * 1), y, size, size,
				game);
		g.drawImage(game.sheetUI.getImage(5), x + (size * 2), y, size, size,
				game);
		g.drawImage(game.sheetUI.getImage(6), x + (size * 3), y, size, size,
				game);
		g.drawImage(game.sheetUI.getImage(7), x + (size * 4), y, size, size,
				game);
		g.drawImage(game.sheetUI.getImage(8), x + (size * 5), y, size, size,
				game);
	}

	private void drawSimulator(Graphics g, int x, int y, int size) {
		g.drawImage(game.sheetUI.getImage(9), x + (size * 1), y, size, size,
				game);
		g.drawImage(game.sheetUI.getImage(10), x + (size * 2), y, size, size,
				game);
		g.drawImage(game.sheetUI.getImage(11), x + (size * 3), y, size, size,
				game);
		g.drawImage(game.sheetUI.getImage(12), x + (size * 4), y, size, size,
				game);
		g.drawImage(game.sheetUI.getImage(13), x + (size * 5), y, size, size,
				game);
		g.drawImage(game.sheetUI.getImage(14), x + (size * 6), y, size, size,
				game);
	}
}
