package unacceptableuse.shoppingsim;

import java.awt.Color;

public class Item {

	private String name;
	private int price, weight, fragility, texpos, height;
	private Color colour;
	private boolean doesCrack = false;
	public String getName() {
		return name;
	}
	public Item setName(String name) {
		this.name = name;
		return this;
	}
	public int getPrice() {
		return price;
	}
	public Item setPrice(int price) {
		this.price = price;
		return this;
	}
	public Item setColour(Color c)
	{
		colour = c;
		return this;
	}
	public Color getColour()
	{
		return colour;
	}
	public int getWeight() {
		return weight;
	}
	public Item setWeight(int weight) {
		this.weight = weight;
		return this;
	}
	public int getFragility() {
		return fragility;
	}
	public Item setFragility(int fragility) {
		this.fragility = fragility;
		return this;
	}
	public int getTexpos() {
		return texpos;
	}
	public Item setTexpos(int texpos) {
		this.texpos = texpos;
		return this;
	}
	public boolean isDoesCrack() {
		return doesCrack;
	}
	public Item setDoesCrack(boolean doesCrack) {
		this.doesCrack = doesCrack;
		return this;
	}
	
	
}
