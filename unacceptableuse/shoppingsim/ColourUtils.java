package unacceptableuse.shoppingsim;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;

public class ColourUtils {

	public static BufferedImage recolourImage(BufferedImage input, Color colour) {
		BufferedImage destination = input;
		WritableRaster destinationRaster = destination.getRaster();
		Raster sourceRaster = input.getRaster();
		

		int[] newColour = {colour.getRed(), colour.getGreen(), colour.getBlue() , colour.getAlpha()};
		
		for(int x = 0; x < sourceRaster.getWidth(); x++)
		{
			for(int y = 0; y < sourceRaster.getHeight(); y++)
			{
				int[] sourcePixel = {0, 0, 0, 0};
				sourceRaster.getPixel(x, y, sourcePixel);
				
				if(sourcePixel[3] != 0)
				{
					destinationRaster.setPixel(x, y, newColour);
				}
				
					
				
				 
			}
		}
		
		
		
		return destination;

	}
}
