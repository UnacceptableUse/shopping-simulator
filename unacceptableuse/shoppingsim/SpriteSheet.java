package unacceptableuse.shoppingsim;

import java.awt.image.BufferedImage;

public class SpriteSheet {


	BufferedImage sheet;
	private BufferedImage[] imagecache;

	public SpriteSheet(BufferedImage sheet, Integer SPRITE_SIZE) {
		this.sheet = sheet;
		int rows = sheet.getWidth() / SPRITE_SIZE;
		int cols = sheet.getHeight() / SPRITE_SIZE;
		imagecache = new BufferedImage[SPRITE_SIZE * SPRITE_SIZE];

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				try{
				imagecache[(i * cols) + j] = sheet.getSubimage(j * SPRITE_SIZE,
						i * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE);
				}catch(ArrayIndexOutOfBoundsException e)
				{
					throw new RuntimeException("Sprite size too small or big?");
				}
			}
		}

	}

	public BufferedImage getImage(int texpos) {

		return imagecache[texpos];
	}

}
