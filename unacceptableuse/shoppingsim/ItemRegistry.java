package unacceptableuse.shoppingsim;

import java.util.ArrayList;

public class ItemRegistry {

	
	private ArrayList<Item> registry = new ArrayList<Item>();
	
	
	public void registerItem(Item i)
	{
		registry.add(i);
	}
	
	public int getRegistrySize()
	{
		return registry.size();
	}
	
	public Item getItem(int pos)
	{
		return registry.get(pos);
	}
}
